<?php

namespace Drupal\Tests\s3fs_sns\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Validate functionality of the s3fs_sns configuration form.
 *
 * @group s3fs_sns
 * @covers \Drupal\s3fs_sns\Form\SettingsForm
 */
class S3fsSnsSettingFormTest extends BrowserTestBase {

  /**
   * A user with administration access.
   *
   * @var \Drupal\user\UserInterface
   */
  protected $adminUser;

  /**
   * {@inheritdoc}
   */
  protected static $modules = ['s3fs_sns'];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();
    $this->adminUser = $this->drupalCreateUser([
      'administer site configuration',
      'administer s3fs_sns configuration',
    ]);
    $this->drupalLogin($this->adminUser);
  }

  /**
   * Test the s3fs_sns config form.
   */
  public function testS3fsSnsConfigForm() {
    $this->drupalGet('admin/config/system/s3fs-sns');
    $this->assertSession()->statusCodeEquals(200);
    $page = $this->getSession()->getPage();
    $this->assertTrue($page->hasButton('Add another'));
    $this->assertTrue($page->hasButton('Save configuration'));

    $edit['edit-topics-arn-0'] = 'arn:aws:sns:us-east-1:222524823419:drupal-sns-test';
    $this->submitForm($edit, 'Save configuration');
    $page = $this->getSession()->getPage();
    $this->assertTrue($page->hasContent('The configuration options have been saved.'));
  }

}
