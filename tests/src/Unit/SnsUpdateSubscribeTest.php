<?php

namespace Drupal\Tests\s3fs_sns\Unit;

use Aws\Sns\Message;
use Drupal\amazon_sns\Event\SnsEvents;
use Drupal\amazon_sns\Event\SnsMessageEvent;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Query\Merge;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\s3fs_sns\EventSubscriber\S3fsSnsSubscriber;
use Drupal\Tests\UnitTestCase;

/**
 * Test S3FS SNS processing of amazon_sns events.
 *
 * @group s3fs_sns
 * @covers \Drupal\s3fs_sns\EventSubscriber\S3fsSnsSubscriber
 */
class SnsUpdateSubscribeTest extends UnitTestCase {

  /**
   * Base message data to be manipulated for each test.
   *
   * This message does not have a valid signature.
   *
   * @var array
   */
  protected array $data;

  /**
   * An array containing the event message.
   *
   * This data needs to be converted to JSON and injected into
   * $this->data['Message'],
   *
   * @var \array[][]
   */
  protected array $messageArray;

  /**
   * Drupal Json Serialization class.
   *
   * @var \Drupal\Component\Serialization\Json
   */
  protected Json $jsonService;

  /**
   * Mock of the Drupal Cache Service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $cacheMocker;

  /**
   * Mock of the Drupal Lock Service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $lockMock;

  /**
   * Mock of the Drupal Database Connection.
   *
   * @var \Drupal\Core\Database\Connection|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $connectionMock;

  /**
   * Mock of SQL Merge query.
   *
   * @var mixed|\PHPUnit\Framework\MockObject\MockObject|\Drupal\Core\Database\Query\Merge
   */
  protected $merge;

  /**
   * Mock of Delete Query.
   *
   * @var \PHPUnit\Framework\MockObject\MockObject|\Drupal\Core\Database\Query\Delete
   */
  protected $delete;

  /**
   * An AWS SNS Message to process.
   *
   * @var \Aws\Sns\Message
   */
  protected Message $message;

  /**
   * Stub for the Config Factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface|\PHPUnit\Framework\MockObject\MockBuilder
   */
  protected $configFactoryStub;

  /**
   * Mock for FileSystem service.
   *
   * @var \Drupal\Core\File\FileSystemInterface|mixed|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $fileSystemMock;

  /**
   * Mock for StreamWrapperManager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface|mixed|\PHPUnit\Framework\MockObject\MockObject
   */
  protected $wrapperManagerMock;

  /**
   * {@inheritdoc}
   */
  public function setup(): void {
    parent::setup();

    // This message does not have a valid signature.
    $this->data = [
      "Type" => "SubscriptionConfirmation",
      "MessageId" => "f93edf3e-bee9-57f3-8752-8e97b283e829",
      "TopicArn" => "arn:aws:sns:us-east-1:222524823419:drupal-sns-test",
      "Timestamp" => "2017-05-31T18:23:38.935Z",
      "SignatureVersion" => "1",
      "Signature" => "D7g1ZmCjj41EsrKlDiRunlS8AsUbI009XScwGKOAryWmCP2wDCb1j7ZR3LDJpkM9ayZRwx5NQMZ18NKnji0iE6Lw5DCGzC93fVKXy7IdZWeApg7gfuXeOu9FpxzjuaY03kbkSzKDWMdJjO0DgBXsJXoUi2gi0AD4ED+yutn7hkDYjW9tq5SzJP9XRp4fXhEDPi1DEP8luNnfyDUcSxvKCFiOaHlkTnps1bvorT5Kr6dmVS/RKf70LNTSKi8bsF/oFGHHAIQJ687OtW2Id0cxVtaPSNnPvf/z9IecZFpflvQEHsqdaC20eAmnP376sAoeAqFsEo81aUxmPXCMDYOPqg==",
      "SigningCertURL" => "https://sns.us-east-1.amazonaws.com/SimpleNotificationService-b95095beb82e8f6a046b3aafc7f4149a.pem",
      "SubscribeURL" => "",
      "UnsubscribeURL" => "https://sns.us-east-1.amazonaws.com/?Action=Unsubscribe&SubscriptionArn=arn:aws:sns:us-east-1:222524823419:drupal-sns-test:84a1d410-a187-44b9-b611-e82307fceb87",
      "Token" => "a token",
    ];

    $this->messageArray = [
      'Records' => [
        0 => [
          'eventVersion' => '2.1',
          'eventSource' => 'aws:s3',
          'awsRegion' => 'us-east-1',
          'eventTime' => '2021-10-01T06:05:58.501Z',
          'eventName' => 'ObjectCreated:Put',
          'userIdentity' => [
            'principalId' => 'AR6L7BRAIN8T3F',
          ],
          'requestParameters' => [
            'sourceIPAddress' => '192.0.2.10',
          ],
          'responseElements' => [
            'x-amz-request-id' => 'GTZFHZW3HA1DKDDT',
            'x-amz-id-2' => 'GHLMUGGATFDXO3OVBTTJXCVD3MVI57AQYV7XQGS2HC7692CATRGC152HII8CI2ZE2WHZYAI8WGTBDKZ3E2S81BXZB61XOX18',
          ],
          's3' => [
            's3SchemaVersion' => '1.0',
            'configurationId' => 'Test SNS Notification',
            'bucket' => [
              'name' => 'test-bucket',
              'ownerIdentity' => [
                'principalId' => 'A2USFBFO7WGZED',
              ],
              'arn' => 'arn:aws:s3:::s3fs-sns-test-sns-notification',
            ],
            'object' => [
              'key' => 'Screenshot.png',
              'size' => 215568,
              'eTag' => 'e12ee85f518d1acf8e10ccd7d8f95528',
              'versionId' => 'Jv55fx7dKMV3Vr.OFFTrFeypwNFM.oX1',
              'sequencer' => '006156A548F82794A3',
            ],
          ],
        ],
      ],
    ];

    $this->jsonService = new Json();

    $this->cacheMocker = $this->getMockBuilder(CacheBackendInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->lockMock = $this->getMockBuilder(LockBackendInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->lockMock->method('acquire')->willReturnOnConsecutiveCalls(FALSE, TRUE);

    // Mock last processed event database record.
    $statement = $this->getMockBuilder('Drupal\Core\Database\Driver\sqlite\Statement')
      ->disableOriginalConstructor()
      ->getMock();
    $statement->expects($this->any())
      ->method('fetchAssoc')
      ->willReturn(['sequence' => '005156A548F82794A3']);

    $select = $this->getMockBuilder('Drupal\Core\Database\Query\Select')
      ->disableOriginalConstructor()
      ->getMock();
    $select->expects($this->any())
      ->method('fields')
      ->will($this->returnSelf());
    $select->expects($this->any())
      ->method('condition')
      ->will($this->returnSelf());
    $select->expects($this->any())
      ->method('execute')
      ->will($this->returnValue($statement));

    // Base mock for allowing the recorded events table update.
    $this->merge = $this->getMockBuilder('Drupal\Core\Database\Query\Merge')
      ->disableOriginalConstructor()
      ->getMock();
    $this->merge->expects($this->any())
      ->method('key')
      ->willReturn($this->returnSelf());
    $this->merge->expects($this->any())
      ->method('fields')
      ->willReturn($this->returnSelf());
    $this->merge->expects($this->any())
      ->method('execute')
      ->willReturn(Merge::STATUS_UPDATE);

    // Base mock for allowing deleting files from the metadata table.
    $this->delete = $this->getMockBuilder('Drupal\Core\Database\Query\Delete')
      ->disableOriginalConstructor()
      ->getMock();
    $this->delete->expects($this->any())
      ->method('condition')
      ->willReturn($this->returnSelf());
    $this->delete->expects($this->any())
      ->method('execute')
      ->willReturn(1);

    // Database Connection mock base. Provide select support, merge support
    // provided outside setUp();
    $this->connectionMock = $this->getMockBuilder('Drupal\Core\Database\Connection')
      ->disableOriginalConstructor()
      ->getMock();
    $this->connectionMock = $this->getMockBuilder('Drupal\Core\Database\Connection')
      ->disableOriginalConstructor()
      ->getMock();
    $this->connectionMock->expects($this->any())
      ->method('select')
      ->will($this->returnValue($select));

    // Module Config.
    $moduleSettings = [
      's3fs_sns.settings' => [
        'arns' => ['arn:aws:sns:us-east-1:222524823419:drupal-sns-test'],
      ],
      's3fs.settings' => [
        'bucket' => 'test-bucket',
      ],
    ];
    $this->configFactoryStub = $this->getConfigFactoryStub($moduleSettings);

    $this->fileSystemMock = $this->getMockBuilder(FileSystemInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->fileSystemMock->method('dirname')->willReturnCallback(
      function ($uri) {
        $last = strrpos($uri, '/');
        $uri = substr_replace($uri, '', $last + 1);
      }
    );

    $storageMock = $this->getMockBuilder(StreamWrapperInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    $this->wrapperManagerMock = $this->getMockBuilder(StreamWrapperManagerInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $this->wrapperManagerMock->method('getViaScheme')
      ->willReturn($storageMock);

  }

  /**
   * Validate that the subscriber has correctly subscribed for notifications.
   */
  public function testSubscribedEvents() {
    $subscriber = new S3fsSnsSubscriber(
      $this->configFactoryStub,
      $this->jsonService,
      $this->lockMock,
      $this->connectionMock,
      $this->fileSystemMock,
      $this->wrapperManagerMock,
      $this->cacheMocker
    );

    $events = $subscriber->getSubscribedEvents();

    $this->assertArrayHasKey(SnsEvents::NOTIFICATION, $events, 'Subscribed to SNS Notification');
    $this->assertEquals(100, $events[SnsEvents::NOTIFICATION][1], 'Verify we are a higher priority subscriber');

  }

  /**
   * Perform a basic test with a stock valid message.
   */
  public function testBasicTest() {

    $this->setupForValidMessage();

    $subscriber = new S3fsSnsSubscriber(
      $this->configFactoryStub,
      $this->jsonService,
      $this->lockMock,
      $this->connectionMock,
      $this->fileSystemMock,
      $this->wrapperManagerMock,
      $this->cacheMocker
    );

    $this->assertInstanceOf(S3fsSnsSubscriber::class, $subscriber, 'Subscriber is a S3fsSnsSubscriber');
    $subscriber->processSnsUpdate(new SnsMessageEvent($this->message));
  }

  /**
   * Ensure we do not process unapproved Topic ARN's.
   */
  public function testUnapprovedArn() {
    $this->data['TopicArn'] = 'arn:aws:sns:us-east-1:0000000000:drupal-unapproved-topic-test';
    $this->data['Message'] = $this->jsonService->encode($this->messageArray);
    $this->message = new Message($this->data);

    $this->connectionMock->expects($this->never())
      ->method('merge')
      ->will($this->returnValue($this->merge));

    $subscriber = new S3fsSnsSubscriber(
      $this->configFactoryStub,
      $this->jsonService,
      $this->lockMock,
      $this->connectionMock,
      $this->fileSystemMock,
      $this->wrapperManagerMock,
      $this->cacheMocker
    );

    $subscriber->processSnsUpdate(new SnsMessageEvent($this->message));
  }

  /**
   * Ensure we do not process unknown buckets.
   */
  public function testUnknownBucket() {
    $this->messageArray['Records'][0]['s3']['bucket']['name'] = 'unknown-bucket';
    $this->data['Message'] = $this->jsonService->encode($this->messageArray);
    $this->message = new Message($this->data);

    $this->connectionMock->expects($this->never())
      ->method('merge')
      ->will($this->returnValue($this->merge));

    $subscriber = new S3fsSnsSubscriber(
      $this->configFactoryStub,
      $this->jsonService,
      $this->lockMock,
      $this->connectionMock,
      $this->fileSystemMock,
      $this->wrapperManagerMock,
      $this->cacheMocker
    );

    $subscriber->processSnsUpdate(new SnsMessageEvent($this->message));
  }

  /**
   * Ensure we do not process messages with unsupported versions.
   *
   * @param string $version
   *   Version to simulate for message test.
   *
   * @dataProvider versionProvider()
   */
  public function testUnsupportedVersions(string $version) {
    // The database should not be called to modify the metadata.
    $this->connectionMock->expects($this->never())
      ->method('merge')
      ->will($this->returnValue($this->merge));

    $subscriber = new S3fsSnsSubscriber(
      $this->configFactoryStub,
      $this->jsonService,
      $this->lockMock,
      $this->connectionMock,
      $this->fileSystemMock,
      $this->wrapperManagerMock,
      $this->cacheMocker
    );

    $this->messageArray['Records'][0]['eventVersion'] = $version;
    $this->data['Message'] = $this->jsonService->encode($this->messageArray);
    $message = new Message($this->data);

    $subscriber->processSnsUpdate(new SnsMessageEvent($message));
  }

  /**
   * Provides a list of unsupported version strings.
   *
   * @return array
   *   Single element array consisting of a string for version test.
   */
  public function versionProvider(): array {
    return [
      'Version too old same major' => ['2.0'],
      'Version major too old' => ['1.2'],
      'Version Major too new' => ['3.2'],
    ];
  }

  /**
   * Ensure we correctly process event types.
   *
   * @param string $eventName
   *   THe s3 event name to test with.
   * @param string $type
   *   Event type, "add" or "delete" otherwise invalid.
   * @param bool $isValid
   *   Is this a supported event type.
   *
   * @dataProvider eventTypesProvider()
   */
  public function testEventTypes(string $eventName, string $type, bool $isValid) {
    $this->messageArray['Records']['0']['eventName'] = $eventName;

    if ($isValid) {
      switch ($type) {
        case 'add':
          $this->connectionMock->expects($this->exactly(2))
            ->method('merge')
            ->will($this->returnValue($this->merge));
          break;

        case 'delete':
          $this->connectionMock->expects($this->exactly(1))
            ->method('delete')
            ->will($this->returnValue($this->delete));
          $this->connectionMock->expects($this->exactly(1))
            ->method('merge')
            ->will($this->returnValue($this->merge));
          break;

      }

      $this->data['Message'] = $this->jsonService->encode($this->messageArray);
      $this->message = new Message($this->data);
    }
    else {
      $this->connectionMock->expects($this->atMost(1))
        ->method('merge')
        ->will($this->returnValue($this->merge));
    }

    $this->data['Message'] = $this->jsonService->encode($this->messageArray);
    $this->message = new Message($this->data);

    $subscriber = new S3fsSnsSubscriber(
      $this->configFactoryStub,
      $this->jsonService,
      $this->lockMock,
      $this->connectionMock,
      $this->fileSystemMock,
      $this->wrapperManagerMock,
      $this->cacheMocker
    );

    $subscriber->processSnsUpdate(new SnsMessageEvent($this->message));
  }

  /**
   * Test conditions provider for testEventTypes()
   *
   * @return array
   *   Keyed by test name containing two elements
   *   - string, Event name.
   *   - bool, is this a supported event.
   */
  public function eventTypesProvider(): array {
    return [
      'ObjectCreated:Put Valid' => ['ObjectCreated:Put', 'add', TRUE],
      'ObjectCreated:Post' => ['ObjectCreated:Post', 'add', TRUE],
      'ObjectCreated:Copy' => ['ObjectCreated:Copy', 'add', TRUE],
      'ObjectCreated:CompleteMultipartUpload' => [
        'ObjectCreated:CompleteMultipartUpload',
        'add',
        TRUE,
      ],
      'ObjectRemoved:Delete' => ['ObjectRemoved:Delete', 'delete', TRUE],
      'ObjectRemoved:DeleteMarkerCreated' => [
        'ObjectRemoved:DeleteMarkerCreated',
        'delete',
        TRUE,
      ],
      'ObjectRemoved:InvalidEvent' => [
        'ObjectRemoved:InvalidEvent',
        'invalid',
        FALSE,
      ],
    ];
  }

  /**
   * Verify that only new events are processed.
   *
   * @param string $newSequence
   *   Sequence to use for the received event.
   * @param bool $isNewer
   *   Should $newSequence be considered newer than $oldSequence.
   *
   * @dataProvider eventSequenceProvider
   */
  public function testEventSequenceOrder(string $newSequence, bool $isNewer) {

    $this->messageArray['Records']['0']['s3']['object']['sequencer'] = $newSequence;
    $this->data['Message'] = $this->jsonService->encode($this->messageArray);
    $this->message = new Message($this->data);

    if ($isNewer) {
      $this->connectionMock->expects($this->exactly(2))
        ->method('merge')
        ->will($this->returnValue($this->merge));
    }
    else {
      $this->connectionMock->expects($this->never())
        ->method('merge');
    }

    $subscriber = new S3fsSnsSubscriber(
      $this->configFactoryStub,
      $this->jsonService,
      $this->lockMock,
      $this->connectionMock,
      $this->fileSystemMock,
      $this->wrapperManagerMock,
      $this->cacheMocker
    );

    $subscriber->processSnsUpdate(new SnsMessageEvent($this->message));
  }

  /**
   * Data provider for testEventSequenceOrder()
   *
   * @return array
   *   An array of test conditions keyed by test name.
   */
  public function eventSequenceProvider(): array {
    // In self::setup() we set the last known event id to '005156A548F82794A3'.
    return [
      'Repeat event' => ['005156A548F82794A3', FALSE],
      'Newer event, same length' => ['007156A548F82794A3', TRUE],
      'Newer event, shorter new sequence' => ['007156A548F82794A', TRUE],
      'Older event, shorter new sequence' => ['004156A548F82794A', FALSE],
      'Newer event, shorter old sequence' => ['006156A548F82794A3A', TRUE],
      'Older event, shorter old sequence' => ['004156A548F82794A3A', FALSE],
    ];
  }

  /**
   * Additional mock setup for messages that should process as valid.
   */
  protected function setupForValidMessage() {
    $this->connectionMock->expects($this->exactly(2))
      ->method('merge')
      ->will($this->returnValue($this->merge));

    $this->data['Message'] = $this->jsonService->encode($this->messageArray);
    $this->message = new Message($this->data);
  }

}
