<?php

namespace Drupal\Tests\s3fs_sns\Unit;

use Drupal\Core\Form\FormStateInterface;
use Drupal\s3fs_sns\Form\SettingsForm;
use Drupal\Tests\UnitTestCase;

/**
 * Test confirming new SNS subscriptions.
 *
 * @group s3fs_sns
 * @covers \Drupal\s3fs_sns\Form\SettingsForm
 */
class SettingsFormValidationTest extends UnitTestCase {

  /**
   * Ensure that Topic ARN validation is correct.
   *
   * @param array $arn
   *   ARN to validate against.
   * @param bool $isValid
   *   Should this ARN be considered Valid.
   *
   * @dataProvider arnTestsProvider()
   */
  public function testArnValidation(array $arn, bool $isValid) {
    $formStateMock = $this->getMockBuilder(FormStateInterface::class)
      ->disableOriginalConstructor()
      ->getMock();

    if ($isValid == TRUE) {
      $formStateMock->expects($this->never())
        ->method('setErrorByName');
    }
    else {
      $formStateMock->expects($this->atLeastOnce())
        ->method('setErrorByName');
    }

    $formStateMock->expects($this->any())
      ->method('getValue')
      ->willReturn(['arn' => $arn]);

    // Module Config.
    $moduleSettings = [
      's3fs_sns.settings' => [
        'arns' => ['arn:aws:sns:us-east-1:222524823419:drupal-sns-test'],
      ],
    ];
    $configFactoryStub = $this->getConfigFactoryStub($moduleSettings);

    $test = new SettingsForm($configFactoryStub);

    $test->setStringTranslation($this->getStringTranslationStub());
    $form = $test->buildForm([], $formStateMock);

    $test->validateForm($form, $formStateMock);

  }

  /**
   * Provides test data for testArnValidation().
   *
   * @return array
   *   Array of test data keyed by test name.
   */
  public function arnTestsProvider() {
    return [
      'Valid full arn' => [
        ['arn:aws:sns:us-east-1:222524823419:drupal-sns-test'],
        TRUE,
      ],
      'AWS GovCloud' => [
        ['arn:aws-us-gov:sns:us-gov-east-1:222524823419:drupal-sns-test'],
        TRUE,
      ],
      'AWS China' => [
        ['arn:aws-cn:sns:cn-north-1:222524823419:drupal-sns-test'],
        TRUE,
      ],
      'Invalid random string' => [
        [$this->getRandomGenerator()->string(25)],
        FALSE,
      ],
      'No ARN prefix' => [
        ['aws:aws:s3:us-east-1:222524823419:drupal-sns-test'],
        FALSE,
      ],
      'Invalid service id' => [
        ['arn:aws:s3:us-east-1:222524823419:drupal-sns-test'],
        FALSE,
      ],
      'No Region id invalid' => [
        ['arn:sns:s3::222524823419:drupal-sns-test'],
        FALSE,
      ],
      'No account id invalid' => [
        ['arn:sns:s3:us-east-1::drupal-sns-test'],
        FALSE,
      ],
      'Should not have path' => [
        ['arn:aws:sns:us-east-1:222524823419:drupal-sns-test/path'],
        FALSE,
      ],
      'Multiple valid ARNs' => [
        [
          'arn:aws:sns:us-east-1:222524823419:drupal-sns-test',
          'arn:aws:sns:us-east-1:223524823419:drupal-valid-test',
        ],
        TRUE,
      ],
      'Mixed valid and invalid ARNs' => [
        [
          'arn:aws:sns:us-east-1:222524823419:drupal-sns-test',
          'arn:aws:sns:us-east-1:223524823419:drupal-valid-test',
          $this->getRandomGenerator()->string(25),
        ],
        FALSE,
      ],
    ];
  }

}
