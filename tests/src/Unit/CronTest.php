<?php

namespace Drupal\Tests\s3fs_sns\Unit;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Tests\UnitTestCase;

/**
 * Test s3fs_sns_cron() hook.
 *
 * @group s3fs_sns
 * @covers s3fs_sns_cron()
 */
class CronTest extends UnitTestCase {

  /**
   * {@inheritdoc}
   */
  public function setup(): void {
    parent::setup();

    $timeServiceMock = $this->getMockBuilder(TimeInterface::class)
      ->disableOriginalConstructor()
      ->getMock();
    $timeServiceMock->method('getRequestTime')
      ->willReturn(1630000000);

    $delete = $this->getMockBuilder('Drupal\Core\Database\Query\Delete')
      ->disableOriginalConstructor()
      ->getMock();
    $delete->expects($this->once())
      ->method('condition')
      ->with('timestamp', '1629913600', '<')
      ->will($this->returnSelf());
    $delete->expects($this->once())
      ->method('execute')
      ->willReturn(1);

    // Database Connection mock base. Provide select support, merge support
    // provided outside setUp();
    $connectionMock = $this->getMockBuilder('Drupal\Core\Database\Connection')
      ->disableOriginalConstructor()
      ->getMock();
    $connectionMock->expects($this->once())
      ->method('delete')
      ->will($this->returnValue($delete));

    // Set the container with the services.
    \Drupal::unsetContainer();
    $container = new ContainerBuilder();
    $container->set('datetime.time', $timeServiceMock);
    $container->set('database', $connectionMock);

    \Drupal::setContainer($container);
  }

  /**
   * Call s3fs_sns_cron to test for execution.
   */
  public function testHookCron() {
    include dirname(__FILE__) . '/../../../s3fs_sns.module';
    s3fs_sns_cron();
  }

}
