<?php

namespace Drupal\s3fs_sns\EventSubscriber;

use Drupal\amazon_sns\Event\SnsEvents;
use Drupal\amazon_sns\Event\SnsMessageEvent;
use Drupal\Component\Serialization\SerializationInterface;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Database\Connection;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Lock\LockBackendInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Drupal\s3fs\S3fsServiceInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * S3fs SNS Update event subscriber.
 *
 * Subscribe to SNS notifications for processing AWS S3 bucket update
 * notifications to keep the s3fs_file table in sync with the actual bucket
 * state.
 */
class S3fsSnsSubscriber implements EventSubscriberInterface {

  /**
   * List of all S3 Event types supported by this subscriber.
   *
   * @var array
   */
  protected array $supportedEvents = [
    'ObjectCreated:Put',
    'ObjectCreated:Post',
    'ObjectCreated:Copy',
    'ObjectCreated:CompleteMultipartUpload',
    'ObjectRemoved:Delete',
    'ObjectRemoved:DeleteMarkerCreated',
  ];

  /**
   * List of events for when a file is created/added to bucket.
   *
   * @var array
   */
  protected array $createdEvents = [
    'ObjectCreated:Put',
    'ObjectCreated:Post',
    'ObjectCreated:Copy',
    'ObjectCreated:CompleteMultipartUpload',
  ];

  /**
   * List of events for when a file has been 'deleted' from the bucket.
   *
   * @var array
   */
  protected array $deletedEvents = [
    'ObjectRemoved:Delete',
    'ObjectRemoved:DeleteMarkerCreated',
  ];

  /**
   * S3fs_sns module settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $config;

  /**
   * Database Connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected Connection $connection;

  /**
   * Lock service.
   *
   * @var \Drupal\Core\Lock\LockBackendInterface
   */
  protected LockBackendInterface $lock;

  /**
   * Json Serialization service.
   *
   * @var \Drupal\Component\Serialization\SerializationInterface
   */
  protected SerializationInterface $serializer;

  /**
   * S3fs Settings.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected ImmutableConfig $s3fsConfig;

  /**
   * Drupal FileSystemService interface.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * Drupal StreamWrapperManager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected StreamWrapperManagerInterface $streamWrapperManager;

  /**
   * Cache that stores S3fs metadata entries.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $s3fsCache;

  /**
   * Constructor for S3fsSnsSubscriber.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $configFactory
   *   Config factory service.
   * @param \Drupal\Component\Serialization\SerializationInterface $serialization
   *   Serialization service.
   * @param \Drupal\Core\Lock\LockBackendInterface $locker
   *   Lock service.
   * @param \Drupal\Core\Database\Connection $connection
   *   Database connection.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   Drupal FileSystemService.
   * @param \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface $streamWrapperManager
   *   Drupal StreamWrapperManager service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $s3fsCacheBin
   *   Cache bin for s3fs module.
   */
  public function __construct(ConfigFactoryInterface $configFactory, SerializationInterface $serialization, LockBackendInterface $locker, Connection $connection, FileSystemInterface $fileSystem, StreamWrapperManagerInterface $streamWrapperManager, CacheBackendInterface $s3fsCacheBin) {
    $this->config = $configFactory->get('s3fs_sns.settings');
    $this->s3fsConfig = $configFactory->get('s3fs.settings');
    $this->serializer = $serialization;
    $this->lock = $locker;
    $this->connection = $connection;
    $this->fileSystem = $fileSystem;
    $this->streamWrapperManager = $streamWrapperManager;
    $this->s3fsCache = $s3fsCacheBin;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      // Try and get the file entry into the database as soon as possible.
      SnsEvents::NOTIFICATION => ['processSnsUpdate', 100],
    ];
  }

  /**
   * Amazon_sns event notification handler.
   *
   * Callback for SnsEvents::NOTIFICATION event.
   *
   * Validate the message is for an approved Topic ARN, a supported event, and
   * is a supported event version prior to processing each individual record.
   *
   * @param \Drupal\amazon_sns\Event\SnsMessageEvent $event
   *   Amazon SNS event.
   *
   * @see https://docs.aws.amazon.com/AmazonS3/latest/userguide/notification-content-structure.html
   * @see https://docs.aws.amazon.com/AmazonS3/latest/userguide/notification-how-to-event-types-and-destinations.html
   */
  public function processSnsUpdate(SnsMessageEvent $event) {
    $rawMessage = $event->getMessage();
    $message = $this->serializer->decode($rawMessage['Message']);

    $approvedArns = $this->config->get('arns');
    if (!in_array($rawMessage['TopicArn'], $approvedArns)) {
      // This is not an approved ARN.
      return;
    }

    foreach ($message['Records'] as $record) {

      // AWS recommends verifying the major and minor versions to be sure all
      // fields exist.
      [$versionMajor, $versionMinor] = explode('.', $record['eventVersion'], 2);
      if ($versionMajor != 2  || $versionMinor < 1) {
        continue;
      }

      if (!in_array($record['eventName'], $this->supportedEvents)) {
        // Not an event type that we need to process, skip.
        continue;
      }
      $this->processRecord($record);
    }

  }

  /**
   * Processes a single file event record.
   *
   * Takes a single pre-validated event record, manages record locking, and
   * event order continuity.
   *
   * @param array $record
   *   A single record from event message.
   */
  protected function processRecord(array $record) {

    if ($record['s3']['bucket']['name'] != $this->s3fsConfig->get('bucket')) {
      // Bucket name doesn't match the s3fs bucket.
      return;
    }

    $path = urldecode($record['s3']['object']['key']);
    $sequence = $record['s3']['object']['sequencer'];
    $lockName = "s3fs_sns_" . hash('sha256', $path);

    // Require a lock to avoid sequence collisions.
    if (!$this->lock->acquire($lockName, 1)) {
      $this->lock->wait($lockName);
      $this->processRecord($record);
    }
    else {
      // Lock obtained, process the message.
      if ($this->isEventNew($path, $sequence)) {
        if (in_array($record['eventName'], $this->createdEvents)) {
          $this->addRecord($record);
        }
        elseif (in_array($record['eventName'], $this->deletedEvents)) {
          $this->deleteRecord($record);
        }
        $this->recordEvent($path, $sequence);
      }

      $this->lock->release($lockName);
    }
  }

  /**
   * Add/update a file record in s3fs metadata cache.
   *
   * @param array $record
   *   An AWS Record result set.
   */
  protected function addRecord(array $record) {

    $timestamp = date('U', strtotime($record['eventTime']));

    $uri = $this->pathToUri(urldecode($record['s3']['object']['key']));

    // Only process if URI is not empty.
    if (!empty($uri)) {
      $metadata = [
        'uri' => $uri,
        'filesize' => $record['s3']['object']['size'],
        'timestamp' => $timestamp,
        'dir' => 0,
        'version' => $record['s3']['object']['versionId'],
      ];

      $this->connection->merge('s3fs_file')
        ->key('uri', $metadata['uri'])
        ->fields($metadata)
        ->execute();

      // Clear this URI from the Drupal cache, to ensure the next read isn't
      // from a stale cache entry.
      $cid = 's3fs:uri' . $metadata['uri'];
      $this->s3fsCache->delete($cid);

      $dirname = $this->fileSystem->dirname($metadata['uri']);
      // Let the streamWrapper handle the folder creation.
      // We use 's3' since it is always available.
      $this->streamWrapperManager->getViaScheme('s3')->mkdir($dirname, 755, STREAM_MKDIR_RECURSIVE);
    }
  }

  /**
   * Remove file record from s3fs metadata cache.
   *
   * @param array $record
   *   An AWS Record result set.
   */
  protected function deleteRecord(array $record) {

    $uri = $this->pathToUri(urldecode($record['s3']['object']['key']));

    // Only process if URI is not empty.
    if (!empty($uri)) {
      $delete_query = $this->connection->delete('s3fs_file')
        ->condition('uri', $uri, '=');

      // Clear URI from the Drupal cache.
      $cid = 's3fs:uri:' . $uri;
      $this->s3fsCache->delete($cid);

      $delete_query->execute();
    }
  }

  /**
   * Determine if this event is newer than the last processed event.
   *
   * @param string $path
   *   The full path of the object in the bucket.
   * @param string $sequence
   *   The sequencer provided in event notification.
   *
   * @return bool
   *   True if event is newer than last processed event, else False.
   */
  protected function isEventNew(string $path, string $sequence): bool {
    $uri = $this->pathToUri($path);

    // If the URI is empty it is either malformed or out of path roots. We can
    // ignore the request by declaring it is not new.
    if (empty($uri)) {
      return FALSE;
    }

    $result = $this->connection->select('s3fs_sns', 's')
      ->fields('s', ['sequence'])
      ->condition('uri', $uri, '=')
      ->execute()
      ->fetchAssoc();

    if (!$result) {
      return TRUE;
    }

    // AWS requires that the strings be right zero padded before comparison.
    $length = max(strlen($result['sequence']), strlen($sequence));
    $previousSequence = str_pad($result['sequence'], $length, '0');
    $eventSequence = str_pad($sequence, $length, '0');

    if (strcmp($eventSequence, $previousSequence) > 0) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Store event sequence for this path as latest to be processed.
   *
   * @param string $path
   *   The full path of the object in the bucket.
   * @param string $sequence
   *   The sequencer provided in event notification.
   */
  protected function recordEvent(string $path, string $sequence) {
    $uri = $this->pathToUri($path);

    // Failsafe check in case earlier checks were missed.
    if (empty($uri)) {
      return;
    }

    $data = [
      'uri' => $uri,
      'sequence' => $sequence,
      'timestamp' => time(),
    ];

    $this->connection->merge('s3fs_sns')
      ->key('uri', $uri)
      ->fields($data)
      ->execute();
  }

  /**
   * Convert an object key path into a s3fs URI.
   *
   * @param string $path
   *   Object path in s3 bucket.
   *
   * @return string
   *   Drupal URI path for file. Empty if invalid.
   */
  protected function pathToUri(string $path): string {
    $rootFolder = $this->s3fsConfig->get('root_folder');
    if (!empty($rootFolder)) {
      if (strpos($path, $rootFolder) === 0) {
        // Path is in root folder. Strip root folder for later use.
        $path = substr_replace($path, '', 0, strlen($rootFolder) + 1);
      }
      else {
        // The path is not inside the root folder. Ignore it.
        return '';
      }
    }

    // Figure out the scheme based on the key's folder prefix.
    $publicFolderName = !empty($this->s3fsConfig->get('public_folder')) ? $this->s3fsConfig->get('public_folder') : 's3fs-public';
    $privateFolderName = !empty($this->s3fsConfig->get('private_folder')) ? $this->s3fsConfig->get('private_folder') : 's3fs-private';
    if (strpos($path, "$publicFolderName/") === 0) {
      // Remove the folder prefix before setting public:// scheme.
      $path = substr_replace($path, '', 0, strlen($publicFolderName) + 1);
      $uri = "public://$path";
    }
    elseif (strpos($path, "$privateFolderName/") === 0) {
      // Remove the folder prefix before setting private:// scheme.
      $path = substr_replace($path, '', 0, strlen($privateFolderName) + 1);
      $uri = "private://$path";
    }
    else {
      // No special prefix means it's an s3:// file.
      $uri = "s3://$path";
    }

    if (mb_strlen($uri) > S3fsServiceInterface::MAX_URI_LENGTH) {
      // Filename is too long and can't be saved.
      return '';
    }

    return $uri;
  }

}
