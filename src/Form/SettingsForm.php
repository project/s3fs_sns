<?php

namespace Drupal\s3fs_sns\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure S3fs SNS Update module settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 's3fs_sns_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['s3fs_sns.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('s3fs_sns.settings')->get();

    $form['topics'] = [
      '#type' => 'fieldset',
      '#prefix' => '<div id="topics-fieldset-wrapper">',
      '#suffix' => '</div>',
      '#title' => $this->t("Monitored ARN's"),
      '#description' => $this->t("List all SNS Topic ARN's that should be monitored. Empty fields will be deleted."),
      '#tree' => TRUE,
    ];

    if (empty($form_state->get('maxFieldsCount'))) {
      $arnConfig = $config['arns'];
      $maxFields = count($arnConfig) + 1;
    }
    else {
      $arnConfig = [];
      foreach ($form_state->cleanValues()->getValues()['topics']['arn'] as $value) {
        if (!empty($value)) {
          $arnConfig[] = $value;
        }
      }
      $maxFields = count($arnConfig) + 1;
    }

    for ($i = 0; $i < $maxFields; $i++) {
      $value = $arnConfig[$i] ?? '';
      $form['topics']['arn'][$i] = [
        '#type' => 'textfield',
        '#title' => $this->t('ARN'),
        '#default_value' => $value,
      ];
    }

    $form['topics']['addtopic'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add another'),
      '#ajax' => [
        'callback' => [$this, 'addAdditionalElementCallback'],
        'event' => 'click',
        'wrapper' => 'topics-fieldset-wrapper',
      ],
      '#submit' => [
        [$this, 'addAdditionalElementSubmitForm'],
      ],

    ];

    $form_state->set('maxFieldsCount', $maxFields);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    foreach ($form_state->getValue('topics')['arn'] as $key => $value) {
      // ARN must match 'arn:<partition>:sns:<region>>:<account-id><resource>'.
      if (!empty($value) && !preg_match('%^arn:[^:\n]*:sns:[^:\n]+:[^:\n]+:[^:\/\n]*$%', $value)) {
        $form_state->setErrorByName("topics][arn][${key}", "ARN invalid");
      }
    }
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $arnValues = [];

    foreach ($form_state->cleanValues()->getValues()['topics']['arn'] as $value) {
      if (!empty($value)) {
        $arnValues[] = $value;
      }
    }

    $this->config('s3fs_sns.settings')
      ->set('arns', $arnValues)
      ->save();

    $form_state->set('maxFieldsCount', NULL);

    // Clear user input.
    $input = $form_state->getUserInput();
    // We should not clear the system items from the user input.
    $clean_keys = $form_state->getCleanValueKeys();
    $clean_keys[] = 'ajax_page_state';
    foreach ($input as $key => $item) {
      if (!in_array($key, $clean_keys) && substr($key, 0, 1) !== '_') {
        unset($input[$key]);
      }
    }
    $form_state->setUserInput($input);
    // Rebuild the form state values.
    $form_state->setRebuild();
    $form_state->setStorage([]);

    parent::submitForm($form, $form_state);
  }

  /**
   * Ajax callback for adding additional ARN fields.
   *
   * @param array $form
   *   A drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal form state.
   *
   * @return array
   *   Updated 'topics' form element.
   */
  public function addAdditionalElementCallback(array &$form, FormStateInterface $form_state) {
    return $form['topics'];
  }

  /**
   * Submit handler to rebuild form when adding additional ARN's.
   *
   * @param array $form
   *   A drupal form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Drupal form state.
   */
  public function addAdditionalElementSubmitForm(array &$form, FormStateInterface $form_state) {
    $maxKey = $form_state->get('maxFieldsCount');

    $form_state->set('maxFieldsCount', $maxKey + 1);

    $form_state->setRebuild();
  }

}
