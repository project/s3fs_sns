CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Installation
* Configuration
* Maintainers

INTRODUCTION
------------
The S3FS SNS Metadata Update module provides a method to keep the s3fs module
metadata table in sync with the AWS bucket by using SNS notifications to ensure
bucket modifications that occur outside of the Drupal instance are reflected in
the local s3fs metadata table.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/s3fs_sns

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/s3fs_sns

REQUIREMENTS
------------

This module requires the following modules:

* [s3fs](https://www.drupal.org/project/s3fs)
* [amazon_sns](https://www.drupal.org/project/amazon_sns)

This module requires that the bucket(s) to be monitored are hosted on AWS S3 and
are configured to provide SNS notifications for at least the following event
Types:

* s3:ObjectCreated:Put
* s3:ObjectCreated:Post
* s3:ObjectCreated:Copy
* s3:ObjectCreated:CompleteMultipartUpload
* s3:ObjectRemoved:Delete
* s3:ObjectRemoved:DeleteMarkerCreated

INSTALLATION
------------

* Install as you would normally install a contributed Drupal module. Visit
  https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

* Configure an SNS topic at AWS to receive s3 events.

* Ensure that the SNS topic has been subscribed with the amazon_sns module.

* Configure AWS to provide SNS notifications for least the events listed under
  the REQUIREMENTS sections of this guide. For more details please
  see [Amazon S3 Event Notifications](https://docs.aws.amazon.com/AmazonS3/latest/userguide/NotificationHowTo.html)

* Configure the list of ARN topics for s3fs_sns to monitor at Administration »
  Configuration » S3FS SNS

MAINTAINERS
-----------

Current maintainers:

* Conrad Lara (cmlara) - https://www.drupal.org/u/cmlara
